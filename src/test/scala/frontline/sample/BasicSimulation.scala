/*
 * Copyright 2011-2018 GatlingCorp (https://gatling.io)
 *
 * All rights reserved.
 */

package frontline.sample

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class BasicSimulation extends Simulation {

  val httpConf = http
    .baseUrl("https://gaed-aws.e-public.net/0620-reg")

  val scn = scenario("scenario1")
    .exec(
      http("Page 0")
        .get("/")
    )

  /* https://gatling.io/docs/current/general/simulation_setup/ */
  setUp(
    scn.inject(
        rampUsersPerSec(10) to 120 during (8 minutes),
  )
  ).protocols(httpConf)
}
